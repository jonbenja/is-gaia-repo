package com.intensostudios.gaia.core.tileset

import com.intensostudios.gaia.core.types.GaiaGdxTypes.TileCode
import com.intensostudios.gaia.eng.defs.GaiaEngDefs._

import com.intensostudios.gaia.core.types.GaiaGdxTypes._

/**
  * Created by ubuntu on 08/01/17.
  */
class BasicTileset extends Tileset {
  /**
    * Translate the tile code to the name for getting from the HashMap
    *
    * @param tileCode
    * @return
    */
  override def get(tileCode: TileCode): String = {
    tileCode match {
      case WALL => "rock1"
      case FLOOR => "wood1"
      case _ => throw new RuntimeException(s"Unknow TileCoe: $tileCode")
    }
  }
}
