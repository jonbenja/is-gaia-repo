package com.intensostudios.gaia.core.tileset

import com.intensostudios.gaia.core.types.GaiaGdxTypes.TileCode

/**
  * Created by ubuntu on 08/01/17.
  */
trait Tileset {

  /**
    * Translate the tile code to the name for getting from the HashMap
    *
    * @param tile
    * @return
    */
  def get(tile: TileCode): String
}
