package com.intensostudios.gaia.core.types

import com.badlogic.gdx.graphics.g2d.TextureRegion

/**
  * Created by ubuntu on 08/01/17.
  */
object GaiaGdxTypes {

  val BLOCK = 64

  type SpriteSheet = Array[Array[TextureRegion]]
  type TileCode = Byte

}
