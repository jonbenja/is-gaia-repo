package com.intensostudios.gaia.core.assets

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.g2d.{TextureAtlas, TextureRegion}
import com.intensostudios.gaia.core.types.GaiaGdxTypes._

import scala.collection.mutable

/**
  * Created by ubuntu on 08/01/17.
  */
object ISAssetLoader {

  lazy val manager = new AssetManager

  lazy val sprites = new mutable.HashMap[String, SpriteSheet]
  lazy val tiles = new mutable.HashMap[String, TextureRegion]


  def load(): Unit = {
    manager.load("packs/humans.pack", classOf[TextureAtlas])
  }

  def waitUntilFinishedLoading(): Unit = {
    manager.finishLoading()
  }

  def prepare(): Unit = {
    createSprite("humans", "peasant", 7)
    createTile("tiles","wood1")
    createTile("tiles","rock1")
  }

  private def createTile(packName: String, tileName: String): Unit = {
    val atlas = manager.get[TextureAtlas](s"packs/$packName.pack")
    tiles.put(tileName,findAndFlip(atlas,tileName))
  }

  private def findAndFlip(atlas: TextureAtlas, name: String): TextureRegion = {
    val region = atlas.findRegion(name)
    if (region == null) throw new RuntimeException(s"Can't find region: $name")
    region.flip(false, true)
    region
  }

  private def createSprite(packName: String, spriteName: String, count: Int): Unit = {
    val atlas = manager.get[TextureAtlas](s"packs/$packName.pack")
    for (i <- 1 to count) {
      val fullName = s"$spriteName$i"
      val region = atlas.findRegion(fullName).split(64, 64)
      region.foreach(l => l.foreach(_.flip(false, true)))
      sprites.put(fullName, region)
    }
  }

}
