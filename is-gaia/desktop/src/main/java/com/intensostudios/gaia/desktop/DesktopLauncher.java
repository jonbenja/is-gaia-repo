package com.intensostudios.gaia.desktop;

/**
 * Created by ubuntu on 08/01/17.
 */

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.intensostudios.gaia.core.gdx.GaiaGame;

public class DesktopLauncher {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = 1920;
        config.height = 1080;
        config.foregroundFPS = 30;
        config.vSyncEnabled = false;
        //confisdg.samples=2;
        config.backgroundFPS = 30;
        config.useGL30 = false;
        new LwjglApplication(new GaiaGame(), config);
    }
}