package com.intensostudios.gaia.eng.gen.dungeon

import org.scalatest.FunSuite

/**
  * Created by ubuntu on 08/01/17.
  */
class DungeonFactory$Test extends FunSuite {

  test("testMaze") {

    DungeonFactory.maze(70, 50).prettyPrint()
  }

  test("testDrunkenWalk") {
    DungeonFactory.drunkenWalk(70, 50, .7f).prettyPrint()
  }

  test("testRoomGen") {
    DungeonFactory.roomGen(70, 50, 15, 15).prettyPrint()
  }

}
