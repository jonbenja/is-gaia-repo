package com.intensostudios.gaia.eng.testutils

import com.intensostudios.gaia.eng.gen.dungeon.DungeonFactory
import com.intensostudios.gaia.eng.util.DunUtils

/**
  * Created by ubuntu on 14/01/17.
  */
object TestByteMapGen extends App {

  genDrunkenWalks()

  def genDrunkenWalks(): Unit = {

    for (i <- 1 to 5) {
      val bm = DungeonFactory.drunkenWalk(100, 80).byteMap()
      DunUtils.writeToFile(bm, s"../drunken_walk_$i.txt")
    }
  }
}