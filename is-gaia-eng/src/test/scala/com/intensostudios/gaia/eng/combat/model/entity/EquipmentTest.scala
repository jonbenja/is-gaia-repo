package com.intensostudios.gaia.eng.combat.model.entity

import com.intensostudios.gaia.eng.combat.model.CombatModel.{ArmData, Magical, Physical, WepData}
import org.scalatest.FunSuite

/**
  * Created by ubuntu on 08/01/17.
  */
class EquipmentTest extends FunSuite {

  test("changeChest") {

    val equipment = new Equipment{}

    val arm1 = ArmData("Chest1", 10, 1, 1.5f)
    val arm2 = ArmData("Chest2", 20, 2, 5.5f)

    assert(equipment.changeChest(arm1).isEmpty)
    assert(equipment.chest.get == arm1)
    println(equipment.chest.get)
    assert(equipment.changeChest(arm2).get == arm1)
    assert(equipment.chest.get == arm2)
    println(equipment.chest.get)

  }


  test("changeWeapon") {
    val equipment = new Equipment{}

    val wep1 = WepData("wep1", 10, "1d6", 1f, Physical)
    val wep2 = WepData("wep2", 20, "1d10", 1f, Magical)

    assert(equipment.changeWeapon(wep1).isEmpty)
    assert(equipment.weapon.get == wep1)
    println(equipment.weapon.get)
    assert(equipment.changeWeapon(wep2).get == wep1)
    assert(equipment.weapon.get == wep2)
    println(equipment.weapon.get)

  }
}
