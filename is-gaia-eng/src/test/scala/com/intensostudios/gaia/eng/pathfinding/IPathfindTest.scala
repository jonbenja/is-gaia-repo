package com.intensostudios.gaia.eng.pathfinding

import com.intensostudios.gaia.eng.defs.GaiaEngDefs.ByteMap
import org.scalatest.{BeforeAndAfter, FunSuite}

/**
  * Created by ubuntu on 14/01/17.
  */
class IPathfindTest extends FunSuite with BeforeAndAfter {

  var iph: IPathfind = _

  before {
    iph = new IPathfind {}
  }

  test("isAdjacentTest") {

    assert(iph.isAdjacent(1 -> 2, 2 -> 2))
    assert(iph.isAdjacent(1 -> 2, 1 -> 3))
    assert(iph.isAdjacent(1 -> 2, 0 -> 2))
    assert(iph.isAdjacent(1 -> 2, 1 -> 1))
    assert(iph.isAdjacent(5 -> 2, 4 -> 2))
    assert(!iph.isAdjacent(5 -> 6, 5 -> 8))
    assert(!iph.isAdjacent(5 -> 6, 5 -> 6))
    assert(!iph.isAdjacent(5 -> 6, 7 -> 5))
    assert(!iph.isAdjacent(5 -> -5, -5 -> 5))
  }

  test("calculateHeuristic") {
    assert(iph.calculateHeuristic(5 -> 3, 10 -> 4) == 60)
    assert(iph.calculateHeuristic(5 -> 5, 6 -> 4) == 20)
  }

  test("calculateMoveCost") {
    iph.setMoveCost(6 -> 5, 60)
    assert(iph.calculateMoveCost(7 -> 5, 6 -> 5) == 70)
  }

  test("squareWithLowestScore") {
    iph.setMoveCost(5 -> 5, 40)
    iph.setMoveCost(5 -> 6, 50)
    iph.setHeuristic(5 -> 5, 60)
    iph.setHeuristic(5 -> 6, 60)
    iph.openList += 5 -> 5
    iph.openList += 5 -> 6

    assert(iph.squareWithLowestScore() == 5 -> 5)

  }

  test("walkableAdjacent") {
    val map: ByteMap =
      Array(
        Array(1, 1, 1, 1),
        Array(1, 0, 0, 1),
        Array(1, 0, 0, 1),
        Array(1, 1, 1, 1)).map(_.map(_.toByte))

    assert(iph.walkableAdjacent(map, 1 -> 1).size == 2)
    assert(iph.walkableAdjacent(map, 0 -> 1).size == 1)
  }

  test("buildPath") {
    val a = 1 -> 1
    val b = 1 -> 2
    val c = 2 -> 2

    iph.parents += c -> b
    iph.parents += b -> a

    val path = iph.buildPath(c)
    assert(path.size == 3)
    assert(path.head == a)
    assert(path(1) == b)
    assert(path.last == c)
  }

  //  test("computeAlreadyOnOpen") {
  //
  //    val pos = 2 -> 2
  //    val a = 1 -> 2
  //    val b = 2 -> 1
  //    val c = 2 -> 3
  //    val d = 3 -> 3
  //
  //
  //    val e = 3->3
  //
  //    iph.parents += c -> b
  //    iph.parents += b -> a
  //    iph.setMoveCost(b,10)
  //    iph.setHeuristic(b,10)
  //
  //    iph.computeAlreadyOnOpen(Seq(a,b,c),)
  //
  //  }
}
