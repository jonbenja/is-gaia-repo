package com.intensostudios.gaia.eng.combat.model

import org.scalatest.FunSuite

/**
  * Created by ubuntu on 08/01/17.
  */
class JsonLoader$Test extends FunSuite {

  test("testAllWeapons") {

    val weapons = JsonLoader.allWeapons
    assert(weapons.length == 3)
    weapons.foreach(w => println(w))
  }


  test("testAllArmour") {
    val armour = JsonLoader.allArmour
    assert(armour.length == 3)
    armour.foreach(println)
  }


  test("allEntities") {
    val entities = JsonLoader.allEntities
    assert(entities.length == 1)
    entities.foreach(println)
  }
}
