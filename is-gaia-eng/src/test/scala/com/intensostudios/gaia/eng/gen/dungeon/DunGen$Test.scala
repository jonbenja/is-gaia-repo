package com.intensostudios.gaia.eng.gen.dungeon

import com.intensostudios.gaia.eng.util.DunUtils
import org.scalatest.FunSuite

import scala.io.Source

/**
  * Created by ubuntu on 14/01/17.
  */
class DunGen$Test extends FunSuite {

  test("testFromByteMap") {

    val bm = DunUtils
      .readFromString(Source.fromInputStream(
        getClass
          .getClassLoader
          .getResourceAsStream(s"dungeons/drunken_walk_1.txt"))("UTF-8").mkString)

    val dg = DunGen.fromByteMap(bm)
    dg.prettyPrint()
  }

}
