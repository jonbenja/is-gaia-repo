package com.intensostudios.gaia.eng.pathfinding

import com.intensostudios.core.utils.IO
import com.intensostudios.gaia.eng.defs.GaiaEngDefs._
import com.intensostudios.gaia.eng.gen.dungeon.DunGen
import com.intensostudios.gaia.eng.util.DunUtils
import org.scalatest.FunSuite
// scala is imported implicitly
import scala.io.Source

/**
  * Created by ubuntu on 14/01/17.
  */
class AStarPathfinderTest extends FunSuite with IPathfind {


  test("findPaths") {

    val bm = bmps.head
    val se = positions.head
    val start: XY = se._1
    val end: XY = se._2
    println(s"Start: $start End: $end Width: ${bm.length} Height: ${bm.head.length}")
    assert(get(bm, start) == FLOOR)
    assert(get(bm, end) == FLOOR)

    val path = new AStarPathfinder().findPath(bm, start, end).get
    validate(path)

    val dg = DunGen.fromByteMap(bm)
    dg.addPath(path)
    dg.prettyPrint()

  }


  def bmps: Seq[ByteMap] = {
    for (i <- 1 to 5) yield {
      DunUtils
        .readFromString(Source.fromInputStream(
          getClass
            .getClassLoader
            .getResourceAsStream(s"dungeons/drunken_walk_$i.txt"))("UTF-8").mkString)
    }
  }

  def positions: Seq[(XY, XY)] = {

    val props = IO.readProperties("dungeons/start_ends.properties")
    val s: XY = props.getProperty("dw1_start").split(",").head.toInt ->
      props.getProperty("dw1_start").split(",").last.toInt
    val e: XY = props.getProperty("dw1_end").split(",").head.toInt ->
      props.getProperty("dw1_end").split(",").last.toInt
    Seq(s -> e)
  }

  def validate(path: XYSeq): Unit = {
    for (i <- 0 until path.size - 1) {
      //      println(s"A: ${path(i)} B: ${path(i + 1)}")
      assert(Math.abs(path(i)._1 - path(i + 1)._1) + Math.abs(path(i)._2 - path(i + 1)._2) == 1)

    }
    //    println("Path OK..")
  }
}
