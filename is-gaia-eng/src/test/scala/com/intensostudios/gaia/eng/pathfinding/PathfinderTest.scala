package com.intensostudios.gaia.eng.pathfinding

import com.intensostudios.gaia.eng.defs.GaiaEngDefs.XYSeq
import com.intensostudios.gaia.eng.gen.dungeon.DungeonFactory
import com.typesafe.scalalogging.LazyLogging
import org.scalatest.FunSuite

/**
  * Created by ubuntu on 08/01/17.
  */
class PathfinderTest extends FunSuite with LazyLogging {


  test("basicPathFind") {

    val dun = DungeonFactory.drunkenWalk(70, 50)
    dun.prettyPrint()
    val start = dun.randomFloor()
    val end = dun.randomFloor()

    println(s"Start: $start End: $end")

    val pathOp = new Pathfinder(dun.byteMap()).findPath(start, end)

    if (pathOp.isDefined) {
      pathOp.get.foreach(f => println(s"${f._1} x ${f._2}"))
      val path = pathOp.get

      dun.addPath(path)
        .prettyPrint()

      validate(path)

    } else {
      println("No path found?")
      dun.addPath(Seq(start, end))
        .prettyPrint()
    }
  }


  test("pathFindPerformance") {
    val num = 500
    val dun = DungeonFactory.drunkenWalk(150, 150, .6f)
    val time = System.currentTimeMillis()
    val paths = for (i <- 1 to num) yield {
      val start = dun.randomFloor()
      val end = dun.randomFloor()
      new Pathfinder(dun.byteMap()).findPath(start, end)
    }
    val timeTaken = System.currentTimeMillis() - time
    logger.debug(s"Time Taken to find $num paths: $timeTaken millis")
    logger.debug(s"Successful Path Finds: ${paths.flatten.size}")
    dun.prettyPrint()
    paths.flatten.foreach(validate)

  }


  def validate(path: XYSeq): Unit = {
    for (i <- 0 until path.size - 1) {
      println(s"A: ${path(i)} B: ${path(i + 1)}")
      assert(Math.abs(path(i)._1 - path(i + 1)._1) + Math.abs(path(i)._2 - path(i + 1)._2) == 1)

    }
    println("Path OK..")
  }


}
