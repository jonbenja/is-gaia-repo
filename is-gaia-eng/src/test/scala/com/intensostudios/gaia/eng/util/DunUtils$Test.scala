package com.intensostudios.gaia.eng.util

import com.intensostudios.gaia.eng.gen.dungeon.DungeonFactory
import org.scalatest.FunSuite

/**
  * Created by ubuntu on 14/01/17.
  */
class DunUtils$Test extends FunSuite {

  test("testReadWriteToString") {

    val bm = DungeonFactory.drunkenWalk(20, 10).byteMap()
    val str = DunUtils.writeToString(bm)
    println(str)
    println("\n**************\n")
    val bm2 = DunUtils.readFromString(str)
    val str2 = DunUtils.writeToString(bm2)
    println(str2)
    bm.zip(bm2).foreach(a => {
      assert(a._1 sameElements a._2)
    })
  }

  test("testReadWriteToFile") {

    val bm = DungeonFactory.drunkenWalk(10, 20).byteMap()
    val path = "../testmap.txt"
    DunUtils.writeToFile(bm, path)
    val bm2 = DunUtils.readFromFile(path)
    bm.zip(bm2).foreach(a => {
      assert(a._1 sameElements a._2)
    })

  }
}
