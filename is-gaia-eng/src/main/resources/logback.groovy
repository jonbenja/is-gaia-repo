

appender("CONSOLE", ConsoleAppender) {
    encoder(PatternLayoutEncoder) {
        pattern = "%d{HH:mm:ss.SSS} %-5level %logger{0} - %msg%n"
    }
}

appender("FILE", FileAppender) {
  file = "is-logs/is.log"
  encoder(PatternLayoutEncoder) {
    pattern = "%d{HH:mm:ss.SSS} %level %logger{35} - %msg%n"
  }
}

root(WARN, ["CONSOLE", "FILE"])

logger("com.intensostudios", DEBUG)


