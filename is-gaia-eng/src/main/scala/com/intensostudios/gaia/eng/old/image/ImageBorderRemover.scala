package com.intensostudios.gaia.eng.old.image

import java.awt.image.{BufferedImage, DataBufferByte}
import java.io.File
import javax.imageio.ImageIO

import com.intensostudios.core.logging.ISLogger

/**
  * Created by jwright on 16/07/16.
  */
object ImageBorderRemover extends App with ImageUtils with ISLogger {

  val inDir = "/home/jwright/Gdx/FullAtlas/"


  val images = new File(inDir).listFiles().filter(_.getName.contains("grass")).map(fi => {
    logger.debug(s"Reading file: ${fi.getAbsolutePath}")
    fi.getName -> ImageIO.read(fi)
  })

  images.foreach(img => {
    removeBorder(img._2, img._1)
  })

  def removeBorder(img: BufferedImage, name: String): Unit = {
    logger.debug(s"Removing Border for $name")

    val width = img.getWidth
    val height = img.getHeight
    val pixels = img.getRaster.getDataBuffer.asInstanceOf[DataBufferByte].getData
    println(s"Pixels width: $width height: $height expected pix:${width * height * 4}: Actual Pix: ${pixels.size}")
    var foundX1 = false
    var foundY1 = false
    var foundX2 = false
    var foundY2 = false
    var x1Off = 0
    var x2Off = 0
    var y1Off = 0
    var y2Off = 0
    var arr = new Array[Double](4)
    for (y <- 0 until height) {
      var clear = true
      var i = 0
      for (x <- 0 until width) {
        i += 1
        val pix = img.getRaster.getPixel(x, y, arr)
        val p4 = pix(3)
        if (p4 != 0) clear = false
      }
      if (!clear && !foundY1) {
                logger.debug(s"Found y1Off: $y")
        y1Off = y
        foundY1 = true
      }
      if (clear && foundY1 && !foundY2) {
                logger.debug(s"Found y2Off: $y")
        y2Off = y
        foundY2 = true
      }
    }
    arr = new Array[Double](4)
    //XOff
    for (x <- 0 until width) {
      var clear = true
      var i = 0
      for (y <- 0 until height) {
        i += 1
        val pix = img.getRaster.getPixel(x, y, arr)
        val p4 = pix(3)
        if (p4 != 0) clear = false
      }
      if (!clear && !foundX1) {
        logger.debug(s"Found x1Off: $x")
        x1Off = x
        foundX1 = true
      }
      if (clear && foundX1 && !foundX2) {
        logger.debug(s"Found x2Off: $x")
        x2Off = x
        foundX2 = true
      }
    }
    if (y2Off==0) y2Off = height
    if (x2Off==0) x2Off = width

    logger.debug(s" $name y1Off: $y1Off y2Off: $y2Off x1Off: $x1Off x2Off: $x2Off")
    createImage(inDir + name, x1Off, y1Off, x2Off - x1Off, y2Off - y1Off, name.replace(".png",""))
  }

}
