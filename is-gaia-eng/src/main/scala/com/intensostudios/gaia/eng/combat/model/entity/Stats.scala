package com.intensostudios.gaia.eng.combat.model.entity

/**
  * Created by ubuntu on 08/01/17.
  */
trait Stats {

  protected var hp, maxHp = 10

  def hit(points: Int): Boolean = {
    hp -= points
    isDead
  }

  def heal(points: Int): Unit = {
    hp += points
    if (hp > maxHp) hp = maxHp
  }

  def isDead: Boolean = hp <= 0
}
