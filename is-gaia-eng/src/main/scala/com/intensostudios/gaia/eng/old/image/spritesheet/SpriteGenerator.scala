package com.intensostudios.gaia.eng.old.image.spritesheet

import com.intensostudios.core.logging.ISLogger

import scala.util.Random

/**
  * Created by jwright on 02/07/16.
  */
object SpriteGenerator extends App with ISLogger{


  val genders = Seq("male","female")
//  val skins = Seq("light","tanned","dark","tanned2","orc","red_orc","skeleton",
//  "dark2","darkelf","darkelf2")
  val skins = Seq("light","tanned")

create("human",Seq("light","tanned","tanned2"),100)

  def create(race:String,sk:Seq[String],amount:Int):Unit = {

    for (i <- 1 to amount) {
      try {
        val gen = new RandomSpriteGen
        val gender = random(genders)
        val skin = random(skins)
        val name = s"${race}_${gender}_${skin}_$i"
        logger.debug(s"Creating $name")
        gen.genSoldier(gender, skin, name)
        gen.genPeasant(gender, skin, name)
        gen.genNoble(gender, skin, name)
      } catch {
        case e: Exception => logger.error("Error creating Sprite", e)
      }
    }
  }


  def random(options:Seq[String]) = options(Random.nextInt(options.size))
}
