package com.intensostudios.gaia.eng.combat.model

import com.intensostudios.gaia.eng.combat.model.CombatModel.WepData

/**
  * Created by ubuntu on 08/01/17.
  */
object JsonDAO {

  lazy val weapons: Map[String, WepData] = JsonLoader.allWeapons.groupBy(_.name).map(m => m._1->m._2.head)
}
