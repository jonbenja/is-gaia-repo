package com.intensostudios.gaia.eng.gen.name

import com.intensostudios.gaia.eng.util.Dice

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
  * Created by ubuntu on 07/01/17.
  */
class MarkovChain(order: Int = 1) {

  lazy val table = new mutable.HashMap[String, ArrayBuffer[Char]]


  def load(data: String): Unit = {

    for (i <- 0 until data.length - order) {
      val ab = table.getOrElse(data.substring(i, i + order), new ArrayBuffer[Char])
      ab += data.charAt(i + order)
      table(data.substring(i, i + order)) = ab
    }
  }

  def generate(maxLength: Int = 20): String = {

    val s: String = (0 until maxLength)
      .map(m => table(Dice.random(table.keySet.toSeq).get))
      .flatMap(m => Dice.random(m))
      .filter(p => p != ' ')
      .map(m => m.toString)
      .reduce((a, b) => a + b)
    s

  }
}
