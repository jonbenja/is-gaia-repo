package com.intensostudios.gaia.eng.old.pathfinding.actors

import java.util.concurrent.ConcurrentLinkedQueue

import akka.actor.{Props, TypedActor}
import akka.pattern.ask
import akka.routing.RoundRobinPool
import akka.util.Timeout
import com.intensostudios.core.exceptions.ISLogicException
import com.intensostudios.core.logging.ISLogger

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

/**
  * Created by jwright on 23/07/16.
  */
class ISAsyncServiceImpl[A,B](actorCount:Int, name:String="Generic Actor System", func:A=>B) extends ISAsyncService[A,B] with ISLogger {

  val actors = TypedActor.context.actorOf(Props(new ISWorker(func))
    .withRouter(RoundRobinPool(actorCount)), name = "Actors")
  implicit val timeout:Timeout =  5 minutes

  val queue = new ConcurrentLinkedQueue[B]

  override def addJob(input:A):Unit = {

    val fut =  actors ? input
    fut.onSuccess {
      case b:B => queue.add(b)
      case a:Any => throw new ISLogicException(s"Unknown type received from actor: ${a.getClass.getName}")
    }
    fut.onFailure {
      case e:Exception => logger.error("Actor Work Failed",e)
      case a:Any => throw new ISLogicException(s"Unknown failure type received from actor: ${a.getClass.getName}")
    }
  }

  override def poll: Option[B] = Option(queue.poll())

  override def count: Int = queue.size()

}
