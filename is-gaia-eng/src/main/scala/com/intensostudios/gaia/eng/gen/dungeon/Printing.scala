package com.intensostudios.gaia.eng.gen.dungeon

import com.intensostudios.gaia.eng.defs.GaiaEngDefs._

/**
  * Created by ubuntu on 20/12/16.
  */
trait Printing {

  val FLOOR_CHAR = '.'
  val WALL_CHAR = '#'
  val HOR_WALL_CHAR = '_'
  val VER_WALL_CHAR = '|'
  val DOOR_CHAR = '+'
  val START_PATH_CHAR = 'S'
  val END_PATH_CHAR = 'E'
  val PATH_CHAR = 'P'

  /**
    * Print the map to the console.
    */
  def printMap(bm: ByteMap): Unit = {
    for (y <- bm.head.indices) {
      for (x <- bm.indices) {
        print(bm(x)(y) + " ")
      }
      println
    }
    println
  }

  /**
    * Print the map to the console.
    */
  def prettyPrintMap(bm: ByteMap): Unit = {
    for (y <- bm.head.indices) {
      for (x <- bm.indices) {
        val v = bm(x)(y)
        var c: Char = ' '
        v match {
          case WALL => {
            if (x == 0 || x == bm.length - 1) c = VER_WALL_CHAR
            else if (y == 0 || y == bm.head.length - 1) c = HOR_WALL_CHAR
            else c = WALL_CHAR
          }
          case FLOOR => c = FLOOR_CHAR
          case DOOR => c = DOOR_CHAR
          case PATH => c = PATH_CHAR
          case START_PATH => c = START_PATH_CHAR
          case END_PATH => c = END_PATH_CHAR
          case _ => throw new RuntimeException(s"Unknown Type: $v")
        }
        print(c + " ")
      }
      println
    }
    println
  }
}
