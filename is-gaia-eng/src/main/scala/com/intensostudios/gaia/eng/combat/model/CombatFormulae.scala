package com.intensostudios.gaia.eng.combat.model

import com.intensostudios.gaia.eng.combat.model.CombatModel.AttackResult
import com.intensostudios.gaia.eng.combat.model.entity.Entity
import com.intensostudios.gaia.eng.util.Dice
import com.typesafe.scalalogging.LazyLogging

/**
  * Created by ubuntu on 08/01/17.
  */
object CombatFormulae extends LazyLogging {


  def attack(attacker: Entity, defender: Entity): AttackResult = {

    val attackBonus = attacker.attackBonus

    val ac = defender.ac

    val hitRoll = Dice.roll(1, 20)
    logger.debug(s"${attacker.name} Hit Rolls $hitRoll vs ${defender.name} AC: ${defender.ac}")
    if (!checkForHit(hitRoll, attackBonus, ac)) {
      return AttackResult(0, s"${attacker.name} Misses")
    }

    val isCrit = hitRoll == 20
    val dmg = calcDamage(attacker, isCrit)
    val msg = s"${attacker.name} hits for $dmg Damage"

    if (isCrit) AttackResult(dmg, msg + ", Critical Hit!")
    else AttackResult(dmg, msg)
  }

  def calcDamage(attacker: Entity, crit: Boolean): Int = {
    val mult = if (crit) 2f else 1f
    Math.round(attacker.damageRoll * mult)
  }

  def checkForHit(roll: Int, attackBonus: Int, ac: Int): Boolean = {
    if (roll == 20) return true
    else if (roll == 1) return false
    else if (roll + attackBonus >= ac) return true
    false
  }


}
