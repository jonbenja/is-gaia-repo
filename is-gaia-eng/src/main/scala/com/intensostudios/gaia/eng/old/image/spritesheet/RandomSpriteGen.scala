package com.intensostudios.gaia.eng.old.image.spritesheet

import com.intensostudios.core.exceptions.ISLogicException
import com.intensostudios.core.logging.ISLogger
import com.intensostudios.core.utils.IO

import scala.collection.mutable.ArrayBuffer
import scala.util.Random

/**
  * Created by jwright on 07/06/16.
  */
class RandomSpriteGen extends SpriteSheetTrait with ISLogger {

  val rand = new Random


  lazy val paths: Array[String] = IO.cpToString("pngs.txt").split("\n")

  def genRandom(gender: String, skin: String, sheetName: String): Unit = {
    val layers = new ArrayBuffer[String]
    layers += getSheet(gender, s"body/$gender/$skin")
    layers += getSheet(gender, "head")
    layers += getSheet(gender, "eyes", skin)
    layers += getSheet(gender, "ears", skin)
    layers += getSheet(gender, "nose", skin)
    layers += getSheet(gender, "facial")
    layers += getSheet(gender, "hair")
    layers += getSheet(gender, "torso")
    layers += getSheet(gender, "belt")
    layers += getSheet(gender, "hands", "gloves")
    layers += getSheet(gender, "legs")
    layers += getSheet(gender, "feet")
    writeToFile(sheetName, layers.toSeq)
  }

  def genSoldier(gender: String, skin: String, sheetName: String): Unit = {
    val layers = new ArrayBuffer[String]
    layers += getSheet(gender, s"body/$gender/$skin")
    layers += getSheet(gender, "head", random(Seq("helms")))
    layers += getSheet(gender, "eyes", skin)
    layers += getSheet(gender, "ears", skin)
    layers += getSheet(gender, "nose", skin)
    layers += getSheet(gender, "facial")
    layers += getSheet(gender, "hair")
    layers += getSheet(gender, "torso", random(Seq("plate", "chest", "chain")))
    layers += getSheet(gender, "torso/back")
    layers += getSheet(gender, "belt", random(Seq("leather")))
    layers += getSheet(gender, "hands", "bracers")
    layers += getSheet(gender, "legs", random(Seq("armor")))
    layers += getSheet(gender, "feet", "armor")
    if (Random.nextInt(5) == 1)
      layers += getSheet("weapons", random(Seq("both hand", "two_hand")))
    else {
      layers += getSheet(random(Seq(gender, "either")), "weapons", "left hand")
      layers += getSheet(random(Seq(gender, "either")), "weapons", "right hand")

    }
    writeToFile(sheetName + "_soldier", layers.toSeq)
  }

  def genPeasant(gender: String, skin: String, sheetName: String): Unit = {
    val layers = new ArrayBuffer[String]

    layers += getSheet(gender, s"body/$gender/$skin")
    layers += getSheet(gender, "head")
    layers += getSheet(gender, "hair", random(Seq("black", "brunette", "brown", "blonde")))
    layers += getSheet(gender, "eyes", random(Seq("blue", "brown", "green")))
    layers += getSheet(gender, "ears", skin)
    layers += getSheet(gender, "nose", skin)
    layers += getSheet(gender, "torso", random(Seq("shirts", "tunics")))
    layers += getSheet(gender, "legs")
    layers += getSheet(gender, "feet", random(Seq("shoes", "slippers")))
    writeToFile(sheetName + "_peasant", layers.toSeq)
  }

  def genNoble(gender: String, skin: String, sheetName: String): Unit = {
    val layers = new ArrayBuffer[String]

    layers += getSheet(gender, s"body/$gender/$skin")
    if (gender=="female") layers += getSheet(gender, "head","tiaras")
    layers += getSheet(gender, "hair")
    layers += getSheet(gender, "eyes", random(Seq("blue", "brown", "green")))
    layers += getSheet(gender, "ears", skin)
    layers += getSheet(gender, "nose", skin)
    if (gender=="female") layers += getSheet(gender, "accessories")
    layers += getSheet(gender, "torso", random(Seq("robes", "gold","dress")))
    layers += getSheet(gender, "torso/back")
    layers += getSheet(gender, "belt", random(Seq("cloth")))
    layers += getSheet(gender, "hands", random(Seq("bracelet","golden")))
    layers += getSheet(gender, "legs",random(Seq("skirt","robes","pants")))
    layers += getSheet(gender, "feet", random(Seq("shoes")))
    writeToFile(sheetName + "_noble", layers.toSeq)
  }

  private def getSheet(gender: String, item: String, search: String): String = {

    val options = paths
      .filter(_.contains(gender))
      .filter(_.contains(item))
      .filter(_.contains(search))

    if (options isEmpty) {
      logger.debug(s"Couldn't find anything for $item with search term $search")
      return getSheet(gender, item)
    }
    if (options.length == 1) options.head
    Random.shuffle(options.toList).head
  }


  private def getSheet(gender: String, item: String): String = {

    val options = paths
      .filter(_.contains(gender))
      .filter(_.contains(item))

    if (options isEmpty) throw new ISLogicException(s"Couldn't find $item")
    if (options.length == 1) options.head
    Random.shuffle(options.toList).head
  }

  def random(options: Seq[String]) = options(Random.nextInt(options.size))
}
