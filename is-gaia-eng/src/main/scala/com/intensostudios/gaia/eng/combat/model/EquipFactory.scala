package com.intensostudios.gaia.eng.combat.model

import com.intensostudios.gaia.eng.combat.model.CombatModel.{Size, Small, WepData}

/**
  * Created by ubuntu on 08/01/17.
  */
object EquipFactory {


  def getUnarmedWeapon(size: Size): WepData = {
    JsonDAO.weapons(size.toString + " Unarmed")
  }
}
