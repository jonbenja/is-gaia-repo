package com.intensostudios.gaia.eng.gen.dungeon

import com.intensostudios.gaia.eng.defs.GaiaEngDefs._

import scala.util.Random

/**
  * Created by ubuntu on 20/12/16.
  */
trait Surrounding {

  /**
    * Returns a positions 1 square away from the current position.
    *
    * @param pos
    * @param diag
    * @return
    */
  def randomDir(w: Wrapper, pos: XY, diag: Boolean = false): XY = {
    Random.shuffle(filterEdge(w.bm, surrounding(w.bm, pos, 1, diag), w.edges)).head
  }

  def randomWall(w: Wrapper, pos: XY, diag: Boolean = false): Option[XY] = {
    Random.shuffle(filterEdge(w.bm, surrounding(w.bm, pos, 1, diag), w.edges)).find(p => w.bm(p._1)(p._2) == WALL)
  }

  def surrounding(bm: ByteMap, pos: XY, area: Int = 1, diag: Boolean = false): XYSeq = {

    val positions: XYSeq = (1 to area).flatMap(i => {
      nesw(bm, pos, i) ++ (if (diag) diagonal(bm, pos, i) else Seq[XY]())
    })
    filterOutsideArea(bm, positions)
  }

  def nesw(bm: ByteMap, pos: XY, i: Int): XYSeq = {
    Seq((pos._1 - i) -> pos._2, (pos._1 + i) -> pos._2, pos._1 -> (pos._2 - i), pos._1 -> (pos._2 + i))
  }

  def diagonal(bm: ByteMap, pos: XY, i: Int): XYSeq = {
    Seq((pos._1 - i) -> (pos._2 - i), (pos._1 + i) -> (pos._2 - 1), (pos._1 - i) -> (pos._2 - i), (pos._1 + i) -> (pos._2 + i))
  }

  def filterOutsideArea(bm: ByteMap, xys: XYSeq): XYSeq = {
    xys.filter(p => p._1 >= 0 && p._1 < bm.length && p._2 >= 0 && p._2 < bm.head.length)
  }

  /**
    * Filter edge positions (i.e. walls)
    *
    * @param bm
    * @param xys
    * @param edge
    * @return
    */
  def filterEdge(bm: ByteMap, xys: XYSeq, edge: XYSeq): XYSeq = {
    xys.filterNot(edge.contains)
  }

  def countSurroundingFloors(bm: ByteMap, p: XY): Int = countSurrounding(bm, p, FLOOR)

  def countSurroundingWalls(bm: ByteMap, p: XY): Int = countSurrounding(bm, p, WALL)

  def countSurrounding(bm: ByteMap, p: XY, part: Byte): Int = {
    surrounding(bm, p, 1).count(c => bm(c._1)(c._2) == part)
  }
}
