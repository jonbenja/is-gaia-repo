package com.intensostudios.gaia.eng.util

import com.typesafe.scalalogging.LazyLogging

/**
  * Created by ubuntu on 21/12/16.
  */
class StopWatch extends LazyLogging {


  var time: Long = -1l
  var started = false


  def start(): Unit = {
    if (started) {
      println("Stopwatch Already Running, stop first")
      return
    }
    time = System.currentTimeMillis()
    started = true
  }

  def soFar: String = {
    val duration = System.currentTimeMillis() - time
    val durString = (duration.toDouble / 1000d).formatted("%.3f")
    s" $durString Seconds So Far"
  }


  def stop: String = {
    val duration = System.currentTimeMillis() - time
    started = false
    val durString = (duration.toDouble / 1000d).formatted("%.3f")

    s"Total Time Taken $durString Seconds"
  }


}