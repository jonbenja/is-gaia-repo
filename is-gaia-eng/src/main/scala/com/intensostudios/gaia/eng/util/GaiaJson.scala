package com.intensostudios.gaia.eng.util

import com.intensostudios.gaia.eng.combat.model.CombatModel._
import org.json4s._
import org.json4s.JsonDSL._
import org.json4s.native.JsonMethods._
import org.json4s.native.Serialization
import org.json4s.native.Serialization._

/**
  * Created by ubuntu on 08/01/17.
  */
object GaiaJson {


  val serialisers = List(DamageTypeSerialiser, SizeSerialiser)

  implicit val formats: Formats = DefaultFormats ++ serialisers

  //  def unmarshall[T](json:String)(implicit m: Manifest[T]) = parse(json).extract[T]

  def readValue[T](json: String)(implicit m: Manifest[T]): T = read[T](json)


  def writeValue[T <: AnyRef](v: T): String = write(v)

}

case object DamageTypeSerialiser extends CustomSerializer[DamageType](format => ( {

  case JString(str) => str match {
    case "Physical" => Physical
    case "Magical" => Magical
  }
  case JNull => null
  case _ => throw new RuntimeException(s"Unknown type: $format")
}, {
  case Physical => JString("Physical")
  case _ => throw new RuntimeException(s"Unknown type: $format")
}))

case object SizeSerialiser extends CustomSerializer[Size](format => ( {

  case JString(str) => str match {
    case "Large" => Large
    case "Medium" => Medium
    case "Small" => Small
  }
  case JNull => null
  case _ => throw new RuntimeException(s"Unknown type: $format")
}, {
  case Physical => JString("Physical")
  case _ => throw new RuntimeException(s"Unknown type: $format")
}))