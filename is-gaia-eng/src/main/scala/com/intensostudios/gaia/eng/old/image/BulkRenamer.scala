package com.intensostudios.gaia.eng.old.image

import java.io.File
import javax.imageio.ImageIO

import com.intensostudios.core.logging.ISLogger


object BulkRenamer extends App with ISLogger {

  val inDir = "/home/jwright/Gdx/FullAtlas/"
  val outDir = "/home/jwright/Gdx/Splitting/Splitted/"

  val images = new File(inDir).listFiles().filter(_.getName.startsWith("house")).map(fi => {
    logger.debug(s"Reading file: ${fi.getAbsolutePath}")
    fi.getName -> ImageIO.read(fi)
  }).foreach(im => {
    val fileName = outDir+"small_"+im._1
    logger.debug(s"Filename: $fileName")
    ImageIO.write(im._2, "PNG", new File(fileName))
  })


}
