package com.intensostudios.gaia.eng.old.image

import java.awt.image.BufferedImage
import java.io.{ByteArrayInputStream, File}
import javax.imageio.ImageIO
import scala.sys.process._

/**
  * Created by ubuntu on 21/12/16.
  */
object ISImgUtils {


  def readAndOpen(arr: Array[Byte], path: String): Unit = {
    val img = read(arr)
    write(img, path)
    open(path)
  }

  def read(arr: Array[Byte]): BufferedImage = {
    ImageIO.read(new ByteArrayInputStream(arr))
  }


  def write(img: BufferedImage, path: String): Unit = {
    ImageIO.write(img, "jpg", new File(path))
  }


  def open(path: String): Unit = {
    s"gnome-open $path" !
  }
}