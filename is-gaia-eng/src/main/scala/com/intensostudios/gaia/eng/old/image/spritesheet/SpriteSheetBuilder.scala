package com.intensostudios.gaia.eng.old.image.spritesheet

import scala.collection.mutable.ArrayBuffer

/**
  * Created by jwright on 07/06/16.
  */
class SpriteSheetBuilder(gender:String, skin:String) extends SpriteSheetTrait {


  var layers = new ArrayBuffer[String]
  gender(gender)
  skin(skin)


  def gender(name:String):SpriteSheetBuilder = {
    this
  }

  def skin(name:String):SpriteSheetBuilder = {
    layers += s"body/$gender/$skin"
    this
  }

  def eyes(name:String):SpriteSheetBuilder = {
    layers += s"body/$gender/eyes/$name"
    this
  }

  def ears(name:String):SpriteSheetBuilder = {
    layers += s"body/$gender/ears/$name"
    this
  }

  def nose(name:String):SpriteSheetBuilder = {
    layers += s"body/$gender/nose/$name"
    this
  }

  def hair(style:String,colour:String):SpriteSheetBuilder = {
    layers += s"hair/$gender/$style/$colour"
    this
  }

  def head(item:String,name:String):SpriteSheetBuilder = {
    layers += s"head/$item/$gender/$name"
    this
  }


  def writeToFile(sheetName:String):Unit = writeToFile(sheetName,layers)








}
