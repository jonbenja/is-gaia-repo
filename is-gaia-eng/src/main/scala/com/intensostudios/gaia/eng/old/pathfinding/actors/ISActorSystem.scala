package com.intensostudios.gaia.eng.old.pathfinding.actors

import akka.actor.{ActorSystem, TypedActor, TypedProps}

/**
  * Created by jwright on 23/07/16.
  */
class ISActorSystem[A, B](actorCount: Int, name: String = "Generic Actor System", func: A => B) {

  val system = ActorSystem(name)
  val asyncService: ISAsyncService[A, B] =
    TypedActor(system).typedActorOf(TypedProps(
      classOf[ISAsyncService[A, B]],
      new ISAsyncServiceImpl[A, B](actorCount, name, func)))

  def addJob(a: A) = asyncService.addJob(a)

  def poll: Option[B] = {
    asyncService.poll
  }

  def count = asyncService.count
}
