package com.intensostudios.gaia.eng.util

import com.intensostudios.gaia.eng.combat.model.CombatFormulae
import com.intensostudios.gaia.eng.combat.model.entity.{AbsEntity, Entity}
import com.typesafe.scalalogging.LazyLogging

/**
  * Created by ubuntu on 08/01/17.
  */
class BattleSim(sleepTime: Int = 1500) extends LazyLogging {


  private val SLEEP_TIME = sleepTime

  def simulate(e1: AbsEntity, e2: AbsEntity): AbsEntity = {
    val ents = Dice.shuffle(Seq(e1, e2))

    while (true) {
      logger.debug(s"${e1.status}, Attrs: ${e1.data.attrs}, Weapon: ${e1.currentWeapon}")
      logger.debug(s"${e2.status} Attrs: ${e2.data.attrs}, Weapon: ${e2.currentWeapon} ")

      if (attack(ents.head, ents.last))
        return ents.head
      Thread.sleep(SLEEP_TIME)
      if (attack(ents.last, ents.head))
        return ents.last
      Thread.sleep(SLEEP_TIME)
    }

    throw new RuntimeException("Simulation Error...")
  }

  def attack(at: AbsEntity, de: AbsEntity): Boolean = {
    val result = CombatFormulae.attack(at, de)
    de.hit(result.dmg)
    logger.debug(result.comment)
    if (de.isDead) {
      logger.debug(s"${de.name} is Dead! ${at.name} Wins!")
      return true
    }
    false
  }
}