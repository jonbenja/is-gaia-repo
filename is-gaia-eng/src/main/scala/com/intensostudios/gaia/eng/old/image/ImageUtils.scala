package com.intensostudios.gaia.eng.old.image

import java.awt._
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

import com.intensostudios.core.logging.ISLogger

/**
  * Created by jwright on 06/07/16.
  */
trait ImageUtils extends ISLogger {

  val outDir = "/home/jwright/Gdx/Splitting/Splitted/"

  def createImage(baseUrl: String, x: Int, y: Int, w: Int, h: Int, name: String): Unit = {
    logger.debug(s"Creating Image For $baseUrl")
    val existing = ImageIO.read(new File(baseUrl))
    val image = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB)
    val g2d:Graphics2D = image.createGraphics
    g2d.drawImage(existing,0,0,w,h,x,y,x+w,y+h,null)
    g2d.dispose()
    val filename =  outDir + name + ".png"
    logger.debug(s"Filename: $filename")
    ImageIO.write(image, "PNG", new File(filename))
    logger.debug("Image Created")
  }
}
