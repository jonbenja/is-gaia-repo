package com.intensostudios.gaia.eng.pathfinding

import com.intensostudios.gaia.eng.defs.GaiaEngDefs._

import scala.annotation.tailrec
import scala.collection.mutable.ArrayBuffer

/**
  * Created by ubuntu on 14/01/17.
  */
class AStarPathfinder(floor: Byte = FLOOR, wall: Byte = WALL) extends IPathfind {


//  /**
//    * Entry Method
//    *
//    * @param map
//    * @param start
//    * @param end
//    */
//  def findPath(map: ByteMap, start: XY, end: XY): Option[XYSeq] = {
//
//    // 1
//    setMoveCost(start, 0)
//    openList += start
//
//    // 2
//    var walkable = walkableAdjacent(map, start)
//    openList ++= walkable
//    walkable.foreach(f => {
//      parents.put(f, start)
//      gmap.put(f, calculateMoveCost(f, start))
//      hmap.put(f, calculateHeuristic(f, end))
//    })
//
//    // 3
//    openList -= start
//    closedList += start
//    var next = walkable.sortBy(f => calculateF(f)).min
//
//    while (true) {
//      // 4
//      openList -= next
//      closedList += next
//
//      walkable = walkableAdjacent(map, next)
//      walkable = walkable.filter(p => !closedList.contains(p))
//
//      val newSquares = walkable.filter(p => !openList.contains(p))
//      newSquares.foreach(f => {
//        openList += f
//        parents.put(f, next)
//        gmap.put(f, calculateMoveCost(f, next))
//        hmap.put(f, calculateHeuristic(f, end))
//      })
//
//      val oldSquares = walkable.filter(p => openList.contains(p))
//      oldSquares.foreach(f => {
//        val newG = calculateMoveCost(f, next)
//        if (newG < calculateMoveCost(f, parents(f))) {
//          parents.put(f, next)
//          gmap.put(f, calculateMoveCost(f, next))
//          hmap.put(f, calculateHeuristic(f, end))
//        }
//      })
//
//      if (closedList.contains(end)) {
//        logger.debug("Path Found!")
//        return Some(buildPath(end))
//      } else if (openList.isEmpty) {
//        logger.debug("No path..")
//        return None
//      }
//      // 3.5 (LowestF)
//      next = squareWithLowestScore()
//    }
//    throw new RuntimeException("!!!!!!!!!!!!")
//  }

  def findPath(map: ByteMap, start: XY, end: XY): Option[XYSeq] = {
    setMoveCost(start, 0)
    openList += start
    find22(map,start,end)
  }

  @tailrec
  final def find22(map: ByteMap, next: XY, end: XY): Option[XYSeq] = {
    openList -= next
    closedList += next
    val walkable = walkableAdjacent(map, next).filter(p => !closedList.contains(p))
    computeNotOnOpen(walkable.filter(p => !openList.contains(p)), next, end)
    computeAlreadyOnOpen(walkable.filter(openList.contains), next, end)
    if (closedList.contains(end)) {
      return Some(buildPath(end))
    } else if (openList.isEmpty) {
      logger.debug("No path..")
      return None
    }
    find22(map, squareWithLowestScore(), end)
  }

}
