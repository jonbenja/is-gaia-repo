package com.intensostudios.gaia.eng.old.image.spritesheet

import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

/**
  * Created by jwright on 07/06/16.
  */
trait SpriteSheetTrait {

  val baseDir  = "/home/jwright/git/Universal-LPC-spritesheet/"

  val outDir = "/home/jwright/Gdx/GeneratedSheets/"

  def writeToFile(fileName:String,layers:Seq[String]) = {

    val images = layers.map(fp =>ImageIO.read(new File(baseDir+fp+".png")))


    val combined = new BufferedImage(images.head.getWidth,images.head.getHeight,BufferedImage.TYPE_INT_ARGB)

    val g  =combined.getGraphics

    images.foreach(i => {
      g.drawImage(i,0,0,null)
    })

    ImageIO.write(combined,"PNG",new File(outDir+fileName+".png"))
  }
}
