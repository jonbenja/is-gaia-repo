package com.intensostudios.gaia.eng.pathfinding

import com.intensostudios.gaia.eng.defs.GaiaEngDefs.XY

/**
  * Created by ubuntu on 08/01/17.
  */
class PathPos(val x: Int, val y: Int) {

  var g, f, h = 0
  var parent: PathPos = _


  def ==(pos: PathPos): Boolean = x == pos.x && y == pos.y

  def pos: XY = x -> y
}
