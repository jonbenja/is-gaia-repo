package com.intensostudios.gaia.eng.gen.dungeon

import com.intensostudios.gaia.eng.defs.GaiaEngDefs._

/**
  * Created by ubuntu on 20/12/16.
  */
case class Wrapper(bm: ByteMap, all: XYSeq, inner: XYSeq, edges: XYSeq, corners: XYSeq) {

}
