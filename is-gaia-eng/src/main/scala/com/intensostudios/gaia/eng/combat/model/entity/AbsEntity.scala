package com.intensostudios.gaia.eng.combat.model.entity

import com.intensostudios.gaia.eng.combat.model.CombatModel.{Attrs, EntityData, WepData}
import com.intensostudios.gaia.eng.combat.model.EquipFactory
import com.intensostudios.gaia.eng.util.Dice
import com.typesafe.scalalogging.LazyLogging

/**
  * Created by ubuntu on 08/01/17.
  */
abstract class AbsEntity(val data: EntityData) extends Entity with Stats with Equipment with LazyLogging {

  protected lazy val unarmedWeapon: WepData = EquipFactory.getUnarmedWeapon(data.size)

  override def ac: Int = 10 + armourBonus + dexMod + sizeMod

  override def sizeMod: Int = 0

  override def dexMod: Int = data.attrs.dex

  override def name: String = data.name

  override def damageRoll: Int = {
    val wep = currentWeapon
    val times = wep.damage.split("d")(0).toInt
    val max = wep.damage.split("d")(1).toInt
    val roll = Dice.roll(times, max)
    logger.debug(s"$name Damage Rolls $times d $max, gets $roll")
    roll
  }

  def currentWeapon: WepData = weapon.getOrElse(unarmedWeapon)

  def status: String = s"$name: $hp/$maxHp"
}
