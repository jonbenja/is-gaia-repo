package com.intensostudios.gaia.eng.old.pathfinding

import scala.collection.mutable

/**
 * @author User
 */
class AStarPathFinder extends Surroundable{

  def findPath(start: MapPos, end: MapPos, matrix: Array[Array[MapPos]]): mutable.LinkedHashSet[MapPos] = {

    var openList = new mutable.LinkedHashSet[MapPos]
    var closedList = new mutable.LinkedHashSet[MapPos]


    var current = start

    var floors = new mutable.LinkedHashSet[MapPos]

    // 1
    openList += start

    while (true) {

//      Thread.sleep(1000)
      // 2a
      openList.foreach { p => {
        if (p.parent != null) {
          p.g = p.parent.g + 10
        } else {
          p.g = 10
        }
        p.h = getH(p, end)
        p.f = p.g + p.h
      }

      }

      // a
      current = openList.min(Ordering.by((p: MapPos) => p.f))
//      println("Current: " + current.x + " " + current.y)

      //b
      openList -= current
      closedList += current

      // If we have found the end exit the loop
      if (current.x == end.x && current.y == end.y) {
//        println("Found path")
        return getPath(start, current)
      }


      // c

      // i
      floors = getSurrounding(current, matrix)
      floors --= closedList

      floors.foreach { p => {

        // ii
        if (!openList.contains(p)) {
//          println("Not not openlist: " + p.x + " " + p.y)
          openList += p
          p.parent = current
          p.g = p.parent.g + 10
          p.h = getH(p, end)
          p.f = p.g + p.h
//          println("F: " + p.f + " G: " + p.g + " H:" + p.h)
          openList += p
          // iii
        } else {
//          println("Already on openlist: " + p.x + " " + p.y)
          if (p.parent.g + 10< current.g) {
//            println("Better path: " + p.x + " " + p.y)
            p.parent = current
            p.g = p.parent.g + 10
            p.f = p.g + p.h
          }
        }

      }
      }
      // D if openList is empty there is no path
      if (openList.size == 0) {
        println("No path")
        return null
      }
    }

    null
  }

  def getPath(start: MapPos, end: MapPos): mutable.LinkedHashSet[MapPos] = {

    var path = new mutable.LinkedHashSet[MapPos]

    path += end

    var last = end
    var loop = true
    while (loop) {

      if (last != null) {
//        println("Last: " + last.x + " " + last.y)
        path += last
        if (last.x == start.x && last.y == start.y) return path
      } else {
//        println("Last is null")
        loop = false
      }

      last = last.parent
//      Thread.sleep(500)
    }
    path
  }


  /**
   * Gets the G value of a position, which will be 14 if diagonal from the start, and 10 if not.
    *
    * @param pos
   * @param start
   * @return
   */
  def getG(pos: MapPos, start: MapPos) = if (Math.abs(pos.x - start.x) + Math.abs(pos.y - start.y) > 1) 14 else 10

  def getH(pos: MapPos, end: MapPos): Int = (Math.abs(pos.x - end.x) + Math.abs(pos.y - end.y)) * 10


}