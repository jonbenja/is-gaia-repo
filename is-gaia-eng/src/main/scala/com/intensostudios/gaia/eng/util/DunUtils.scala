package com.intensostudios.gaia.eng.util

import com.intensostudios.core.utils.IO
import com.intensostudios.gaia.eng.defs.GaiaEngDefs.ByteMap

/**
  * Created by ubuntu on 14/01/17.
  */
object DunUtils {

  def writeToString(bm: ByteMap): String = {
    bm.transpose
      .map(m => m.mkString(" ") + "\n").mkString
  }

  def readFromString(str: String): ByteMap = {
    val bm: ByteMap = (for (s <- str.split("\n")) yield {
      s.split(" ").map(_.toByte)
    }).transpose

    bm
  }

  def writeToFile(bm: ByteMap, path: String): Unit = {

    IO.writeToFile(writeToString(bm), path)
  }

  def readFromFile(path: String): ByteMap = {
    readFromString(IO.readFile(path))
  }
}
