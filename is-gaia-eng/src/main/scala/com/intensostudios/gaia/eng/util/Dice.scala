package com.intensostudios.gaia.eng.util

import scala.util.Random

/**
  * Created by ubuntu on 20/12/16.
  */
object Dice {

  lazy val rand = new Random()

  def rollD6(times: Int): Int = {
    val rolls = for (i <- 1 to times) yield {
      1 + rand.nextInt(6)
    }
    rolls.sum
  }

  def roll(times: Int, max: Int): Int = {
    val rolls = for (i <- 1 to times) yield {
      1 + rand.nextInt(max)
    }
    rolls.sum
  }

  def shuffle[A](seq: Seq[A]): Seq[A] = rand.shuffle(seq)

  def random[A](options: Seq[A]): Option[A] = if (options.isEmpty) None else Some(options(rand.nextInt(options.size)))


  def between(min: Int, max: Int): Int = {
    min + rand.nextInt((1 + max) - min)
  }

  /**
    * Vary the number by + or - the given percentage.
    *
    * @param value
    * @param pctChange
    * @return
    */
  def vary(value: Int, pctChange: Int): Int = {
    val dif = Math.round(value.toFloat * (pctChange / 100f))
    value + between(-dif, dif)
  }

}
