package com.intensostudios.gaia.eng.combat.model

import com.intensostudios.core.utils.IO
import com.intensostudios.gaia.eng.combat.model.CombatModel._
import com.intensostudios.gaia.eng.util.{GaiaJson, ISJson}
import org.json4s.CustomSerializer
import org.json4s.JsonAST.{JNull, JString}

/**
  * Created by ubuntu on 08/01/17.
  */
object JsonLoader {


  def allWeapons: Array[WepData] = {
    GaiaJson.readValue[AllWeapons](IO.cpToString("json/weapons.json")).weapons
  }

  def allArmour: Array[ArmData] = {
    GaiaJson.readValue[AllArmour](IO.cpToString("json/armour.json")).armour
  }

  def allEntities: Array[EntityData] = {
    GaiaJson.readValue[AllEntities](IO.cpToString("json/entities.json")).entities

  }
}
