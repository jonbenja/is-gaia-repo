package com.intensostudios.gaia.eng.pathfinding

import com.intensostudios.gaia.eng.defs.GaiaEngDefs._
import com.intensostudios.gaia.eng.exception.PathfindingException
import com.intensostudios.gaia.eng.gen.dungeon.Surrounding
import com.typesafe.scalalogging.LazyLogging

import scala.annotation.tailrec
import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
  * Created by ubuntu on 11/01/17.
  */
trait IPathfind extends Surrounding with LazyLogging {

  val floor: Byte = FLOOR
  val wall: Byte = WALL

  val openList = new mutable.LinkedHashSet[XY]
  val closedList = new mutable.LinkedHashSet[XY]

  val gmap = new mutable.HashMap[XY, Int]
  val hmap = new mutable.HashMap[XY, Int]
  val parents = new mutable.HashMap[XY, XY]

  def buildPath(end: XY): XYSeq = {
    val al = new ArrayBuffer[XY]
    var current: XY = end
    while (true) {
      al += current
      val op = parents.get(current)
      if (op.isEmpty) return al.reverse
      current = op.get
      logger.debug(s"Adding $current  to path..")
    }
    throw new RuntimeException("Error building path..")
  }

//  def calculateCosts(seq: XYSeq, pos: XY, end: XY): Unit = {
//    val grouped = seq
//      .filter(p => !closedList.contains(p))
//      .groupBy(openList.contains)
//
//    val notOnOpen: Option[XYSeq] = grouped.get(false)
//    if (notOnOpen.isDefined) {
//      logger.debug(s"New Squares not on open: ${notOnOpen.get.size}")
//      computeNotOnOpen(seq, pos, end)
//    }
//
//    val onOpen = grouped.get(true)
//    if (onOpen.isDefined) {
//      logger.debug(s"New Squares already on open: ${onOpen.get.size}")
//      computeAlreadyOnOpen(seq, pos,end)
//    }
//
//
//  }

  def computeAlreadyOnOpen(seq: XYSeq, pos: XY,end:XY): Unit = {
    seq.foreach(f => {
      val newG = calculateMoveCost(f, pos)
      if (newG < calculateMoveCost(f, parents(f))) {
        parents.put(f, pos)
        gmap.put(f, calculateMoveCost(f, pos))
        hmap.put(f, calculateHeuristic(f, end))
      }
    })
  }

  def computeNotOnOpen(seq: XYSeq, pos: XY, end: XY): Unit = {
    seq.foreach(p => {
      openList += p
      gmap.put(p, calculateMoveCost(p, pos))
      hmap.put(p, calculateHeuristic(p, end))
      parents.put(p, pos)
      logger.debug(s"New Pos: $p G: ${gmap(p)} H: ${hmap(p)} F: ${calculateF(p)}")
    })
  }

  def squareWithLowestScore(): XY = openList.min(Ordering.by((p: XY) => calculateF(p)))

  def calculateF(pos: XY): Int = gmap(pos) + hmap(pos)

  def calculateHeuristic(pos: XY, end: XY): Int = {
    (Math.abs(pos._1 - end._1) + Math.abs(pos._2 - end._2)) * 10
  }

  def calculateMoveCost(pos: XY, parent: XY): Int = {
    if (!isAdjacent(pos, parent)) throw new PathfindingException(s"Pos not adjacent to parent. $pos - $parent")
    gmap(parent) + 10
  }

  def walkableAdjacent(map: ByteMap, xy: XY): XYSeq = {
    surrounding(map, xy)
      .filter(p => get(map, p) == floor)
  }

  def get(map: ByteMap, xy: XY): Byte = map(xy._1)(xy._2)

  def setMoveCost(pos: XY, g: Int): Unit = {
    gmap += pos -> g
  }

  def isAdjacent(p1: XY, p2: XY): Boolean = {
    Math.abs(p1._1 - p2._1) + Math.abs(p1._2 - p2._2) == 1
  }

  def setHeuristic(pos: XY, h: Int): Unit = {
    hmap += pos -> h
  }
}
