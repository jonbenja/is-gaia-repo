package com.intensostudios.gaia.eng.gen.dungeon

import com.typesafe.scalalogging.LazyLogging

/**
  * Created by ubuntu on 20/12/16.
  */
class DrunkWalk(width: Int, height: Int) extends DunGen(width, height) with LazyLogging {


  /**
    * Runs the drunken walk algorithm until X floors are present on the map
    *
    * @param floors
    * @return
    */
  def drunkenWalk(floors: Int): DrunkWalk = {
    fillWithWalls()
    moveRandomDoorPos()
    setPosAsDoor()
    while (countFloors() < floors) {
      if (!moveRandomWall()) {
        moveRandom()
      }
      setPosAsFloor()
    }
    this
  }

  /**
    * Runs the drunken walk algorithm until X percent of internal positions
    * are floors.
    *
    * @param pct
    * @return
    */
  def drunkenWalk(pct: Float): DrunkWalk = {
    fillWithWalls()
    moveRandomDoorPos()
    setPosAsDoor()

    val floors = (w.inner.size.toDouble * pct).toInt
    logger.info(s"Total Positions: ${w.inner.size}  Floors to create: $floors")
    drunkenWalk(floors)
  }

}