package com.intensostudios.gaia.eng.gen.dungeon

import com.intensostudios.gaia.eng.defs.GaiaEngDefs._
import com.intensostudios.gaia.eng.util.Dice
import com.typesafe.scalalogging.LazyLogging

/**
  * Created by ubuntu on 20/12/16.
  */
abstract class DunGen(width: Int, height: Int) extends Positioning
  with Printing with Surrounding with Counting with LazyLogging {

  protected lazy val bm: ByteMap = createByteMap(width, height)
  protected lazy val w = Wrapper(bm, allPositions(bm), innerPositions(bm), edgePositions(bm), cornerPositions(bm))
  protected var pos: XY = _

  def createByteMap(width: Int, height: Int): ByteMap = Array.ofDim(width, height)

  /**
    * Enclose the map with walls.
    *
    * @return
    */
  def enclose(): DunGen = {

    w.edges
      .foreach { case (x, y) => {
        bm(x)(y) = WALL
      }
      }
    this
  }

  def fillWithWalls(): DunGen = {
    w.all.foreach { case (x, y) => bm(x)(y) = WALL }
    this
  }

  def randomInnerPos(): XY = Dice.random(w.inner).get

  def randomFloor(): XY = Dice.random(w.inner.filter(p => byteMap()(p._1)(p._2) == FLOOR)).get

  def randomEdgePos(): XY = Dice.random(w.edges).get

  def randomDoorPos(): XY = Dice.random(w.edges.filterNot(w.corners.contains)).get

  def addPath(path: XYSeq): DunGen = {
    path.foreach(f => {
      byteMap()(f._1)(f._2) = PATH
    })
    byteMap()(path.head._1)(path.head._2) = START_PATH
    byteMap()(path.last._1)(path.last._2) = END_PATH

    this
  }

  /**
    * Set the current position to a floor
    *
    * @return
    */
  def setPosAsFloor(): DunGen = {
    bm(pos._1)(pos._2) = FLOOR
    this
  }

  /**
    * Set the current position to a Door
    *
    * @return
    */
  def setPosAsDoor(): DunGen = {
    bm(pos._1)(pos._2) = DOOR
    this
  }

  /**
    * Set the current position to a Wall.
    *
    * @return
    */
  def setPosAsWall(): DunGen = {
    bm(pos._1)(pos._2) = WALL
    this
  }

  def moveRandomDoorPos(): DunGen = {
    pos = randomDoorPos()
    this
  }

  def moveRandom(): DunGen = {
    pos = randomDir(w, pos)
    this
  }

  def moveRandomWall(): Boolean = {
    val op = randomWall(w, pos)
    if (op.isEmpty) {
      logger.trace("No Nearby wall found..")
      return false
    }
    pos = op.get
    true
  }

  def printPos(): DunGen = {
    println(s"POS: ${pos._1} x ${pos._2}")
    this
  }

  def countFloors(): Int = countFloors(bm)

  def prettyPrint(): DunGen = {
    prettyPrintMap(bm)
    this
  }

  def edges(): XYSeq = w.edges

  def all(): XYSeq = w.all

  def inner(): XYSeq = w.inner

  def byteMap(): ByteMap = bm
}

object DunGen {

  def fromByteMap(bm: ByteMap): DunGen = {
    val dg = new DunGen(bm.length, bm.head.length) {}

    for (x <- bm.indices; y <- bm.head.indices) {
      dg.byteMap()(x)(y) = bm(x)(y)
    }

    dg
  }
}

