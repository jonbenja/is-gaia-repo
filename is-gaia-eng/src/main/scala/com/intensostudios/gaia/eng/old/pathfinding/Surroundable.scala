package com.intensostudios.gaia.eng.old.pathfinding

import com.intensostudios.gaia.eng.defs.GaiaEngDefs._

import scala.collection.mutable

/**
 * Created by jwright on 07/10/2015.
 */
trait Surroundable {

  /**
   * Get surrounding positions that are directly walkable
 *
   * @param pos The position on the matrix
   * @param matrix The matrix to check
   * @return A set of walkable positions
   */
  def getSurroundingDiag(pos: MapPos, matrix: Array[Array[MapPos]]): mutable.LinkedHashSet[MapPos] = {

    var surround = new mutable.LinkedHashSet[MapPos]

    var left = matrix(pos.x - 1)(pos.y)
    var right = matrix(pos.x + 1)(pos.y)
    var top = matrix(pos.x)(pos.y - 1)
    var bottom = matrix(pos.x)(pos.y + 1)
    var topLeft = matrix(pos.x - 1)(pos.y - 1)
    var topRight = matrix(pos.x + 1)(pos.y - 1)
    var bottomLeft = matrix(pos.x - 1)(pos.y + 1)
    var bottomRight = matrix(pos.x + 1)(pos.y + 1)

    if (left.value == FLOOR) surround += left
    if (right.value == FLOOR) surround += right
    if (top.value == FLOOR) surround += top
    if (bottom.value == FLOOR) surround += bottom

    if (topLeft.value == FLOOR && Set(left, top).subsetOf(surround)) surround += topLeft
    if (topRight.value == FLOOR && Set(right, top).subsetOf(surround)) surround += topRight
    if (bottomLeft.value == FLOOR && Set(left, bottom).subsetOf(surround)) surround += bottomLeft
    if (bottomRight.value == FLOOR && Set(right, bottom).subsetOf(surround)) surround += bottomRight

    surround

  }

  /**
   * Get surrounding positions that are directly walkable
 *
   * @param pos The position on the matrix
   * @param matrix The matrix to check
   * @return A set of walkable positions
   */
  def getSurrounding(pos: MapPos, matrix: Array[Array[MapPos]]): mutable.LinkedHashSet[MapPos] = {

    var surround = new mutable.LinkedHashSet[MapPos]

    var left = matrix(pos.x - 1)(pos.y)
    var right = matrix(pos.x + 1)(pos.y)
    var top = matrix(pos.x)(pos.y - 1)
    var bottom = matrix(pos.x)(pos.y + 1)

    if (left.value == FLOOR) surround += left
    if (right.value == FLOOR) surround += right
    if (top.value == FLOOR) surround += top
    if (bottom.value == FLOOR) surround += bottom

    surround

  }

  def getMultSurrounding(pos: MapPos, xplaces: Int, yplaces: Int, matrix: Array[Array[MapPos]], onlyFloors: Boolean): mutable.LinkedHashSet[MapPos] = {

    var surround = new mutable.LinkedHashSet[MapPos]

    for (y <- pos.y - yplaces to pos.y + yplaces if y >= 0 && y < matrix(0).length; 
    x <- pos.x - xplaces to pos.x + xplaces if x >= 0 && x < matrix.length) {

      if (!onlyFloors) surround += matrix(x)(y)

      else if (matrix(x)(y).value == FLOOR) surround += matrix(x)(y)
    }

    surround
  }
}
