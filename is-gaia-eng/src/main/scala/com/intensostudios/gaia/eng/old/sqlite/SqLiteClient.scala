package com.intensostudios.gaia.eng.old.sqlite

import java.sql.{Connection, DriverManager, ResultSet}

/**
  * Created by ubuntu on 21/12/16.
  */
class SqLiteClient {

  lazy val clazz = Class.forName("org.sqlite.JDBC")

  var conn: Connection = _

  def connect(dbName: String): Unit = {
    conn = DriverManager.getConnection(s"jdbc:sqlite:$dbName")
  }

  def exec(sql: String): Unit = {
    val stmt = conn.createStatement()
    stmt.executeUpdate(sql)
    stmt.close()
  }

  def query(sql: String): ResultSet = {
    val stmt = conn.createStatement()
    stmt.executeQuery(sql)
  }

  def close(): Unit = conn.close()
}