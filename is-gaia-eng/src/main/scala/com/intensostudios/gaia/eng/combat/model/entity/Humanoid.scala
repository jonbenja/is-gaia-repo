package com.intensostudios.gaia.eng.combat.model.entity

import com.intensostudios.gaia.eng.combat.model.CombatModel._
import com.intensostudios.gaia.eng.util.Dice

/**
  * Created by ubuntu on 08/01/17.
  */
class Humanoid(data: EntityData) extends AbsEntity(data)  {

  override def attackBonus: Int = 0

  override def armourBonus: Int = armourCount

}