package com.intensostudios.gaia.eng.combat.model.entity

import com.intensostudios.gaia.eng.combat.model.CombatModel.EntityData
import com.intensostudios.gaia.eng.util.Dice

/**
  * Created by ubuntu on 08/01/17.
  */
class Monster(data: EntityData) extends AbsEntity(data) {

  override def armourBonus: Int = 0

  override def attackBonus: Int = 0

}
