package com.intensostudios.gaia.eng.old.image.spritesheet

import java.io.File

import com.intensostudios.core.utils.IO

/**
  * Created by jwright on 07/06/16.
  */
object SpriteSheetFilenameParser extends App{

  val path= "/home/jwright/git/is-scaleng-repo/is-scaleng-core/src/main/resources/pngs.txt"
  val str = IO.readFile(path)

  val out = str.replace(".png","")

  IO.writeToFile(out,new File(path).getAbsolutePath)
//  val baseDir  = "/home/jwright/git/Universal-LPC-spritesheet/"
//
//
//  val outDir = "/home/jwright/Gdx/GeneratedSheets/"
//
//
//  val malePaths = Seq("body/male/dark","body/male/ears/bigears_dark","body/male/eyes/brown","body/male/nose/bignose_dark",
//  "hair/male/bangs","facial/male/beard/black")
//
//  val sheetName = "dark_male"
//
//
//  combine(malePaths,sheetName)
//
//
//  def combine(filePaths:Seq[String],name:String):Unit = {
//
//
//  val images = filePaths.map(fp =>ImageIO.read(new File(baseDir+fp+".png")))
//
//
//    val combined = new BufferedImage(images.head.getWidth,images.head.getHeight,BufferedImage.TYPE_INT_ARGB)
//
//    val g  =combined.getGraphics
//
//    images.foreach(i => {
//      g.drawImage(i,0,0,null)
//    })
//
//    ImageIO.write(combined,"PNG",new File(outDir+name+".png"))
//  }





}
