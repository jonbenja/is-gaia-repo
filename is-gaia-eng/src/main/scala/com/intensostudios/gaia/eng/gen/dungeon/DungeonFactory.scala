package com.intensostudios.gaia.eng.gen.dungeon

/**
  * Created by ubuntu on 08/01/17.
  */
object DungeonFactory {

  def maze(w:Int,h:Int):DunGen = {
    new MazeGen(w,h).createMaze()
  }

  def drunkenWalk(w:Int,h:Int,floorPct:Float=.5f):DunGen = {
    new DrunkWalk(w,h).drunkenWalk(floorPct)
  }

  def roomGen(w:Int,h:Int,roomCount:Int=5,corCount:Int=5):DunGen = {
    new RoomGen(w,h).roomGen(roomCount,corCount)
  }
}
