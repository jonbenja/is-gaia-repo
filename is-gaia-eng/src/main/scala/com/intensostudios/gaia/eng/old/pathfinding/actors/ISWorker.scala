package com.intensostudios.gaia.eng.old.pathfinding.actors

import akka.actor.Actor

/**
  * Created by jwright on 23/07/16.
  */
class ISWorker[A,B](func:A=>B) extends Actor {

  override def receive: PartialFunction[Any, Unit] = {
    case a:A => sender ! func(a)
  }
}
