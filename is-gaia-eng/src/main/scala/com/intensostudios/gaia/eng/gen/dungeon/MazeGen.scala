package com.intensostudios.gaia.eng.gen.dungeon

import com.intensostudios.gaia.eng.defs.GaiaEngDefs.XY
import com.typesafe.scalalogging.LazyLogging

import scala.annotation.tailrec
import scala.collection.mutable
import scala.util.Random

/**
  * Created by ubuntu on 20/12/16.
  */
class MazeGen(width: Int, height: Int) extends DunGen(width, height) with LazyLogging {

  lazy val visitMap: Array[Array[Boolean]] = {
    Array.ofDim[Boolean](width, height)
  }

  lazy val stack = new mutable.Stack[XY]()

  def createMaze(): MazeGen = {
    fillWithWalls()
    pos = randomInnerPos()
    setPosAsFloor()
    setPosAsVisited()
    genMaze()
    this
  }

  @tailrec private def genMaze(): Unit = {
    val pOp = Random.shuffle(surrounding(bm, pos, 1))
      .flatMap(m => filterEdge(bm, Seq(m), edges()).headOption)
      .filter(p => countSurroundingFloors(bm, p) < 2)
      .find(p => !isVisited(p))
    if (pOp.isDefined) {
      val p = pOp.get
      pos = p
      setPosAsFloor()
      setPosAsVisited()
      stack.push(p)
    } else {
      if (!backtrack()) {
        logger.info("Finished Creating Maze..")
        fillDeadEnds()
        return
      }
    }
    genMaze()
  }

  @tailrec private def fillDeadEnds(count: Int = 0): Unit = {
    if (count > 20) return
    val deadEnds = inner().filter(p => countSurroundingWalls(bm, p) == 3)
    if (deadEnds.isEmpty) {
      logger.info("No dead ends left..")
      return
    }
    logger.trace("Filling Dead Ends..")

    deadEnds.foreach(p => {
      pos = p
      setPosAsWall()
    })
    fillDeadEnds(count + 1)
  }

  def backtrack(): Boolean = {
    if (stack.isEmpty) return false
    pos = stack.pop()
    true
  }

  def isVisited(pos: XY): Boolean = visitMap(pos._1)(pos._2)

  def setPosAsVisited(): Unit = {
    visitMap(pos._1)(pos._2) = true
  }
}
