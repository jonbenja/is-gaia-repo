package com.intensostudios.gaia.eng.collision

import com.intensostudios.gaia.eng.defs.GaiaEngDefs.XYF

/**
  * Created by ubuntu on 20/12/16.
  */
case class Circle(centre: XYF, radius: Float) {

}
