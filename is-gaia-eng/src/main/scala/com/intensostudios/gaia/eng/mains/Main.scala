package com.intensostudios.gaia.eng.mains

import com.intensostudios.gaia.eng.gen.name.MarkovChain
import com.typesafe.scalalogging.LazyLogging

/**
  * Created by ubuntu on 20/12/16.
  */
object Main extends App with LazyLogging {
  /*  logger.debug("Hello World")
    logger.info("Hello World")
    logger.trace("Hello World")*/


  val mc = new MarkovChain(2)

  mc.load("Lorem ipsum dolor sit amet, consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et " +
    "dolore magna aliqua ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea " +
    "commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla " +
    "pariatur excepteur sint occaecat cupidatat non proident " +
    "sunt in culpa qui officia deserunt mollit anim id est laborum")

  for (i <- 0 to 10)
    println(mc.generate(10))
}
