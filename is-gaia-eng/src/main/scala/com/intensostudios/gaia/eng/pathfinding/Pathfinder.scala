package com.intensostudios.gaia.eng.pathfinding

import com.intensostudios.gaia.eng.defs.GaiaEngDefs._
import com.intensostudios.gaia.eng.exception.PathfindingException
import com.intensostudios.gaia.eng.gen.dungeon.Surrounding
import com.typesafe.scalalogging.LazyLogging

import scala.annotation.tailrec
import scala.collection.mutable

/**
  * Created by ubuntu on 08/01/17.
  */
class Pathfinder(map: ByteMap, floor: Byte = FLOOR, wall: Byte = WALL) extends LazyLogging
  with Surrounding {

  var openList = new mutable.LinkedHashSet[XY]
  var closedList = new mutable.LinkedHashSet[XY]

  val gValues = new mutable.HashMap[XY, Int]
  val fValues = new mutable.HashMap[XY, Int]
  val parents = new mutable.HashMap[XY, XY]

  /**
    * Finds the shortest path between the 2 points
    *
    * @param start
    * @param end
    * @return
    */
  def findPath(start: XY, end: XY): Option[XYSeq] = {
    if (get(start) == wall) throw new PathfindingException("Start Position is a Wall")
    else if (get(end) == wall) throw new PathfindingException("End Position is a Wall")
    openList += start
    find(start, end)
  }

  /**
    *
    * @param last
    * @param end
    * @return
    */
  @tailrec
  private def find(last: XY, end: XY): Option[XYSeq] = {

    val current = openWithLowestF(last, end)
    logger.debug(s"New Square: $current")
    logger.debug(s"Before: OpenList: ${openList.size} Closed List: ${closedList.size}")
    openList -= current
    closedList += current
    logger.debug(s"After: OpenList: ${openList.size} Closed List: ${closedList.size}")
    if (foundEnd(current, end)) {
      logger.debug("Path Found..")
      return Some(createPath(end))
    }

    val walkable = walkableAdjacent(current)
    logger.debug(s"Found ${walkable.size} walkable..")
    calcForSurrounding(walkable, current, end)

    if (openList.isEmpty) {
      logger.debug("No Path..")
      return None
    }
    find(current, end)
  }

  private def calcForSurrounding(walkable: XYSeq, current: XY, end: XY): Unit = {

    walkable.filter(p => !closedList.contains(p))
      .foreach(s => {

        logger.debug(s"Checking $s")
        if (!openList.contains(s)) {
          logger.debug(s"Adding $s to open list...")
          openList += s
          recalc(s, current, end)
        } else {
          val gv = g(s)
          val hv = h(s, end)
          val fv: Int = gv + hv
          if (fv < fValues(s)) {
            logger.debug(s"POS: $s G: $gv H: $hv F: $fv")
            gValues.put(s, gv)
            fValues.put(s, gv + hv)
            setParent(s, current)
          }
        }
      })
  }

  private def setParent(pos: XY, parent: XY): Unit = {
    if (Math.abs(pos._1 - parent._1) + Math.abs(pos._2 - parent._2) != 1) {
      throw new RuntimeException(s"Bad Parent: $pos $parent")
    }

    parents.put(pos, parent)


  }

  private def recalc(pos: XY, current: XY, end: XY): Unit = {

    val gv: Int = g(pos)
    val hv = h(pos, end)
    val fv: Int = gv + hv
    logger.debug(s"POS: $pos G: $gv H: $hv F: $fv")
    gValues.put(pos, gv)
    fValues.put(pos, fv)
    setParent(pos, current)

  }

  private def walkableAdjacent(xy: XY): XYSeq = {
    surrounding(map, xy)
      .filter(p => get(p) == floor)
  }


  private def createPath(end: XY): XYSeq = {
    logger.debug("Creating Path..")
    closedList
      .filter(p => parents.get(p).isDefined)
      .map(p => parents(p))
      .toSeq ++ Seq(end)
  }

  private def foundEnd(pos: XY, end: XY): Boolean = {
    pos._1 == end._1 && pos._2 == end._2
  }

  private def openWithLowestF(start: XY, end: XY): XY = {
    openList.min(Ordering.by((p: XY) => fValues.getOrElse(p, 0)))
  }

  private def get(xy: XY): Byte = map(xy._1)(xy._2)

  /**
    * Gets the G value of a position, which will be 14 if diagonal from the start, and 10 if not.
    *
    * @param pos
    * @return
    */
  private def g(pos: XY): Int = {
    val parent = parents.get(pos)
    if (parent.isDefined) {
      val pg = gValues(parent.get) + 10
      return pg
    }
    10
  }

  private def h(pos: XY, end: XY): Int = (Math.abs(pos._1 - end._1) + Math.abs(pos._2 - end._2)) * 10
}
