package com.intensostudios.gaia.eng.util

import org.json4s.{DefaultFormats, Formats}
import org.json4s.native.Serialization.{read, write}

/**
  * Created by ubuntu on 08/01/17.
  */
trait ISJson {

  implicit val formats: Formats = DefaultFormats

  def readValue[T](json: String)(implicit m: Manifest[T]) = read[T](json)


  def writeValue[T <: AnyRef](v: T) = write(v)
}
