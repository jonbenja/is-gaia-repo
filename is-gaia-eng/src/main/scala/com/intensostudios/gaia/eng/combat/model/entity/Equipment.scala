package com.intensostudios.gaia.eng.combat.model.entity

import com.intensostudios.gaia.eng.combat.model.CombatModel._

/**
  * Created by ubuntu on 08/01/17.
  */
trait Equipment {

  private var chestSlot: Option[ArmData] = None
  private var weaponSlot: Option[WepData] = None

  def changeChest(arm: ArmData): Option[ArmData] = {
    val current = chestSlot
    chestSlot = Some(arm)
    current
  }

  def chest: Option[ArmData] = chestSlot

  def changeWeapon(wepData: WepData): Option[WepData] = {
    val current = weaponSlot
    weaponSlot = Some(wepData)
    current
  }

  def weapon: Option[WepData] = weaponSlot

  def armourCount: Int = Seq[Option[ArmData]](chestSlot).flatten.map(m => m.ac).sum
}
