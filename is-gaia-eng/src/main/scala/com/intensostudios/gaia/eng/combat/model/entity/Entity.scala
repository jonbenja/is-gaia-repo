package com.intensostudios.gaia.eng.combat.model.entity

import com.intensostudios.gaia.eng.combat.model.CombatModel.{Size, WepData}

/**
  * Created by ubuntu on 08/01/17.
  */
trait Entity {


  def attackBonus: Int

  def ac: Int

  def armourBonus: Int

  def sizeMod: Int

  def dexMod: Int

  def name: String

  def damageRoll: Int

}
