package com.intensostudios.gaia.eng.combat.model

/**
  * Created by ubuntu on 07/01/17.
  */
object CombatModel {

  sealed trait DamageType

  case object Physical extends DamageType

  case object Magical extends DamageType

  sealed trait Size

  case object Large extends Size

  case object Medium extends Size

  case object Small extends Size

  case class AllWeapons(weapons: Array[WepData])

  case class WepData(name: String, cost: Int, damage: String, weight: Float, damageType: DamageType)

  case class AllArmour(armour: Array[ArmData])

  case class ArmData(name: String, cost: Int, ac: Int, weight: Float)

  case class AllEntities(entities: Array[EntityData])

  case class EntityData(name: String, size: Size, attrs: Attrs)

  case class Attrs(str: Int, dex: Int, con: Int, int: Int, spr: Int)

  case class AttackResult(dmg: Int, comment: String)

}
