package com.intensostudios.gaia.eng.old.pathfinding

import com.intensostudios.gaia.eng.defs.GaiaEngDefs._

case class MapPos(var x: Int, var y: Int, var value: Byte) {


  def this(x: Int, y: Int) {
    this(x, y, WALL)
  }

  var parent: MapPos = _

  var g: Float = 0f
  var f: Float = 0f
  var h: Float = 0f

  def pos: XY = x -> y
}