package com.intensostudios.gaia.eng.old.image

import java.io.File
import javax.imageio.ImageIO

/**
  * Created by jwright on 10/07/16.
  */
object AnimalImageSplitter extends App with ImageUtils {


  val inDir = "/home/jwright/Gdx/Images/whtdragons_MVanimals_ALL/animals/"


  val images = new File(inDir).listFiles().map(fi => {
    logger.debug(s"Reading file: ${fi.getAbsolutePath}")
    fi.getName -> ImageIO.read(fi)
  })

  images.foreach { case (name, image) => {
    if (name.endsWith("png")) {
      logger.debug(s"Splitting $name")
      val iw = image.getWidth
      val ih = image.getHeight

      var i = 1
      for (y <- 0 to ih by 192; x <- 0 to iw by 144) {

        createImage(inDir + name, x, y, 144, 192, s"${name.replace(".png","")}$i")
        i += 1
      }
      logger.debug(s"$name split into $i images")
    } else {
      logger.debug(s"Skipping File $name as not a png file")
    }
  }
  }


}
