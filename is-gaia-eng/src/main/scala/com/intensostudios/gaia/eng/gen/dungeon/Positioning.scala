package com.intensostudios.gaia.eng.gen.dungeon

import com.intensostudios.gaia.eng.defs.GaiaEngDefs._

/**
  * Created by ubuntu on 20/12/16.
  */
trait Positioning {

  /**
    * get all coordinates for a map.
    *
    * @return
    */
  def allPositions(bm: ByteMap): XYSeq = {
    for (y <- bm.head.indices; x <- bm.indices) yield x -> y
  }

  def edgePositions(bm: ByteMap): XYSeq = {
    Seq(bm.head.indices.flatMap(y => Seq(0 -> y, (bm.length - 1) -> y)),
      bm.indices.flatMap(x => Seq(x -> 0, x -> (bm.head.length - 1))))
      .flatten
      .distinct
  }

  def innerPositions(bm: ByteMap): XYSeq = {
    allPositions(bm).filterNot(edgePositions(bm).contains).distinct
  }


  def cornerPositions(bm: ByteMap): XYSeq = {
    edgePositions(bm).filter { case (x, y) => {
      ((x == 0 && (y == 0 || y == bm.head.length - 1))
        || (x == bm.length - 1 && (y == 0 || y == bm.head.length - 1)))
    }
    }
  }
}
