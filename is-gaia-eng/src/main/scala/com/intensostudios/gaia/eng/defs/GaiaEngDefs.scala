package com.intensostudios.gaia.eng.defs

/**
  * Created by ubuntu on 20/12/16.
  */
object GaiaEngDefs {

  type XY = (Int,Int)
  type ByteMap = Array[Array[Byte]]
  type XYSeq = Seq[XY]

  type XYF = (Float,Float)




  val FLOOR: Byte = 0
  val WALL: Byte = 1
  val DOOR: Byte = 2
  val UPSTAIRS: Byte = 3
  val DOWNSTAIRS: Byte = 3
  val PATH: Byte = -1
  val START_PATH: Byte = -2
  val END_PATH: Byte = -3
}
