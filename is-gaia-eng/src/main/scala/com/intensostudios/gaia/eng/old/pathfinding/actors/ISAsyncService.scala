package com.intensostudios.gaia.eng.old.pathfinding.actors

/**
  * Created by jwright on 23/07/16.
  */
trait ISAsyncService[A,B] {

  def addJob(input:A):Unit

  def poll:Option[B]

  def count:Int

}
