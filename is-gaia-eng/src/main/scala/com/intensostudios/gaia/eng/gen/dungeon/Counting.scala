package com.intensostudios.gaia.eng.gen.dungeon

import com.intensostudios.gaia.eng.defs.GaiaEngDefs._

/**
  * Created by ubuntu on 20/12/16.
  */
trait Counting {

  def countFloors(bm: ByteMap): Int = bm.flatten.count(_ == FLOOR)

  def countWalls(bm: ByteMap): Int = bm.flatten.count(_ == WALL)
}
