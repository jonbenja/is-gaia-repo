package com.intensostudios.gaia.eng.gen.dungeon

import com.intensostudios.gaia.eng.defs.GaiaEngDefs._
import com.intensostudios.gaia.eng.util.Dice
import com.typesafe.scalalogging.LazyLogging

import scala.annotation.tailrec
import scala.collection.mutable.ArrayBuffer
import scala.util.Random

/**
  * Created by ubuntu on 20/12/16.
  */
class RoomGen(width: Int, height: Int) extends DunGen(width, height) with LazyLogging {

  val rooms = new ArrayBuffer[XYSeq]

  def roomGen(roomCount: Int, corCount: Int): RoomGen = {
    fillWithWalls()
    moveRandomDoorPos()
    setPosAsDoor()
    placeRooms(roomCount)
    placeCorridors(corCount)
    this
  }

  def placeCorridors(corCount: Int): RoomGen = {

    (1 to corCount).foreach(i => {
      logger.trace(s"Creating Corridor $i")
      val startRoom = Random.shuffle(rooms).head
      val endRoom = Random.shuffle(rooms.filter(_ != startRoom)).head
      createCorridor(randomRoomEdge(startRoom), randomRoomEdge(endRoom), startRoom)
    })

    this
  }

  @tailrec private def createCorridor(a: XY, b: XY, startRoom: XYSeq, count: Int = 0): Unit = {
    if (count > 100) return

    val pOp = findNextPoint(a, b, startRoom)
    if (pOp.isEmpty) {
      logger.trace("Finished Corridor")
      return
    }
    val p = pOp.get
    bm(p._1)(p._2) = FLOOR
    createCorridor(p, b, startRoom, count + 1)
  }

  def placeRooms(roomCount: Int): RoomGen = {

    (1 to roomCount).foreach(i => {
      logger.trace(s"Creating Room $i")
      val room = createRoom()
      if (validateRoom(room)) {
        rooms += room
      }
    })

    rooms.flatten.foreach { case (x, y) => {
      bm(x)(y) = FLOOR
    }
    }
    this
  }

  private def createRoom(): XYSeq = {
    val rw = Dice.roll(1, 4)
    val rh = Dice.roll(1, 4)

    val mid = randomInnerPos()
    for (y <- mid._2 - rw to mid._2 + rh; x <- mid._1 - rw to mid._1 + rw) yield x -> y
  }

  private def validateRoom(room: XYSeq): Boolean = {
    if (filterOutsideArea(w.bm, room).size != room.size) {
      logger.trace("Room Outside Map")
      return false
    }
    if (filterEdge(w.bm, room, edges()).size != room.size) {
      logger.trace("Room Hits Edge")
      return false
    }

    if (room.filterNot(rooms.flatten.contains).size != room.size) {
      logger.trace("Room Overlaps Existing Room")
      return false
    }
    true
  }

  private def validatePoint(p: XY, startRoom: XYSeq): Boolean = {
    if (filterOutsideArea(w.bm, Seq(p)).isEmpty) {
      logger.trace("Point Outside Map")
      return false
    }
    if (filterEdge(w.bm, Seq(p), edges()).isEmpty) {
      logger.trace("Point Hits Edge")
      return false
    }

    if (startRoom.contains(p)) {
      logger.trace("Point Overlaps Start Room")
      return false
    }
    true
  }

  //  private def validatePoint(p: XY): Boolean = validateRoom(Seq(p))

  private def randomRoomEdge(room: XYSeq): XY = {
    val xs = Seq(room.map(_._1).min, room.map(_._1).max)
    val ys = Seq(room.map(_._2).min, room.map(_._2).max)
    Random.shuffle(xs).head -> Random.shuffle(ys).head
  }

  private def findNextPoint(a: XY, b: XY, startRoom: XYSeq): Option[XY] = {
    val xdif = a._1 - b._1
    val ydif = a._2 - b._2
    if (xdif == 0 && ydif == 0) {
      logger.info("Found Point")
      return None
    }

    val sur = surrounding(w.bm, a, 1).sortWith { case (i, n) => {
      Math.abs(i._1 - b._1) + Math.abs(i._2 - b._2) < Math.abs(n._1 - b._1) + Math.abs(n._2 - b._2)
    }
    }

    Random.shuffle(sur.take(2)).foreach(s => {
      if (validatePoint(s, startRoom)) return Some(s)
    })

    Random.shuffle(sur.takeRight(2)).foreach(s => {
      if (validatePoint(s, startRoom)) return Some(s)
    })

    logger.trace("Couldn't find point...")
    None
  }
}
