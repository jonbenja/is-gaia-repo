package com.intensostudios.gaia.eng.mains

import com.intensostudios.gaia.eng.combat.model.JsonLoader
import com.intensostudios.gaia.eng.combat.model.entity.{Humanoid, Monster}
import com.intensostudios.gaia.eng.util.BattleSim
import com.typesafe.scalalogging.LazyLogging

/**
  * Created by ubuntu on 08/01/17.
  */
object BattleMain extends App with LazyLogging {

  val bs = new BattleSim(1)

  val winners = for (i <- 0 until 1000) yield {
    val ents = getEnts
    val e1 = ents.head
    val e2 = ents.last
    bs.simulate(e1, e2)
  }

  val pwins = winners.count(_.name == "Player")
  val rwins = winners.count(_.name == "Rat")
  logger.info(s"Player Wins: $pwins Rat Wins: $rwins")

  private def getEnts = JsonLoader.allEntities.map(m => m.name match {
    case "Player" => new Humanoid(m)
    case "Rat" => new Monster(m)
  })
}
