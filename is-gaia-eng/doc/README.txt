Gaia Engine Modules:


Level Generation:
    Drunken Walk
    Dungeon / Room Gen
    World Gen
    Hybrids

Combat:
    Dice / Random Generator
    Combat Formulae + Testing

Data Access:
    SQLite DB for stats

Graphics:
    Dynamic Sprite Generation
    Animation
    Image Cropping / Transparency Util


Basics:
    Collision Masks / Detection
    Entities / Movement

AI:
    Pathfinding
