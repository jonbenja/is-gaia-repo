Combat

Attributes

    Strength:
        Physical Attack Damage
        Max HP

    Dexterity:
        Physical Hit Chance
        Movement Speed

    Constitution:
        Physical Defense
        Health Regen

    Intelligence:
        Magic Attack Damage
        Max MP

    Spirit:
        Magic Defense
        Magic Regen

